## Tamaranga development environment
Usage
- Setup
    - clone this repo
    - `git submodule init`
    - `git submodule update`
    - `mv env-dist .env` and set your project-specific variables
- Build
```sh
docker-compose build 
```
- Run
```
docker-compose up -d
```

# Tools
- Windows
> Update your profile.ps1 with functions, written in `/bin/profile.ps1`
- *nix
> Update your `$PATH` variable with `PATH=$PATH:/path/to/phpenv/bin/` and set environment variable `PROJECT_NAME=phpenv`
- Usage
> Use `pccli` to run command in `php-cli` container.
```sh
    pccli composer update
```
```sh
    pccli php -a
```
```sh
    pccli php -q ./public_html/index.php bff=cron-manager
```
> Use `pfcli` to run in `php-fpm` container.
```sh
   pfcli tail -f ./files/logs/error.log
```
`pfcli` will work only if your current dir mounted in `/var/www/html` in container