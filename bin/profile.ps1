$env:PROJECT_NAME="phpenv"
function pccli {
    $opts="";
    foreach ($arg in $args)
    {
        $opts+=" $arg";
    }
    $opts=$opts -replace '\\', '/'
    & docker run -it --rm --volume "${pwd}:/app" ${env:PROJECT_NAME}_php-cli /bin/sh -c "cd /app && $opts"
}

function pfcli {
    $opts="";
    foreach ($arg in $args)
    {
        $opts+=" $arg";
    }
    $opts=$opts -replace '\\', '/'
    $current_dir=(Get-Item -Path ".\" -Verbose).Name
    & docker exec -it php-fpm sh -c "cd /var/www/html/$current_dir && $opts"
}